﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HS.ED.WebUI.Models;
using HS.ED.WebUI.Infrastructure;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HS.ED.WebUI.Controllers
{
    /// <summary>
    /// Controller will Login/Logout using OWIN
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("index", "directory", null);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string returnUrl, LoginUser user)
        {
            if (ModelState.IsValid)
            {
                // check if the users exits
                AppUser u = UserManager.Find(user.UserName, user.Password);
                if (u == null)
                    ModelState.AddModelError("", "Username or Password was not valid.");
                else
                {
                    ClaimsIdentity identity = await UserManager.CreateIdentityAsync(u, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);
                    return Redirect(returnUrl);
                }

            }

            // go back to the login page
            ViewBag.ReturnUrl = returnUrl;
            return View(user);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthManager.SignOut();
            return RedirectToAction("login", "account", null);
        }

        // helpers
        protected AppUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); }
        }
        protected IAuthenticationManager AuthManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }
	}
}