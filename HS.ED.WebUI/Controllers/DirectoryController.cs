﻿using HS.ED.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using HS.ED.WebUI.Models;
using HS.ED.WebUI.Infrastructure;
using HS.ED.DataRepository.Models;

namespace HS.ED.WebUI.Controllers
{
    /// <summary>
    /// The primary class that does all the work - search/list/edit/delete/create employees
    /// </summary>
    [Authorize]
    public class DirectoryController : Controller
    {
        // instances
        private IEDDataServices _dataService;
        private IEDConfiguration _config;
        private PagingInfo _pagingInfo;
        
        // constructor
        public DirectoryController(IEDDataServices dataService, IEDConfiguration config)
        {
            _dataService = dataService;
            _config = config;
            _pagingInfo = new PagingInfo()
            {
                ItemsPerPage = _config.GetMaxItemsPerPage(),
                MaxPaginationPages = config.GetMaxPaginationPages()
            };
        }

        // GET: /Directory/ 
        // the search page for the directory
        public ActionResult Index()
        {
            return View((object)string.Empty);
        }

        // Get: Directory/Index/{find}
        public ViewResult Search(string find="", int page=1)
        {
            // create a query
            IQueryable<Employee> query = (from e in _dataService.EmployeeRepository
                            where e.Name.ToLower().Contains(find.ToLower())
                             select e).AsQueryable();

            // get the count
            var count = query.Count();

            // get the records per page
            var people = query.OrderBy(e=>e.Name)
                                .Skip((page - 1) * _pagingInfo.ItemsPerPage)
                                .Take(_pagingInfo.ItemsPerPage)
                                .ToList();

            //var people = (from e in _dataService.EmployeeRepository
            //              where e.Name.ToLower().Contains(find.ToLower())
            //              select e).Skip((currentPage - 1) * _pagingInfo.ItemsPerPage)
            //                        .Take(currentPage * _pagingInfo.ItemsPerPage)
            //                        .ToList();

            // determine the paging
            _pagingInfo.TotalItems = count;
            _pagingInfo.CurrentPage = page;

            // check privileges
            AppUser u = UserManager.FindByName(User.Identity.Name);
            bool isAdmin = UserManager.IsInRole(u.Id, "Administrators"); 

            // lets make our view model
            EmployeeViewModel employees = new EmployeeViewModel() { Employees = people, Find = find, PaginationInfo = _pagingInfo, CanCreate = isAdmin, CanEdit = isAdmin };

            return View((object)employees);
        }

        // Get: /Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeModel employee)
        {
            if (ModelState.IsValid)
            {
                // lets add this user to our database
                Employee e = new Employee()
                {
                    Name = employee.Name,
                    Email = employee.Email,
                    JobTitle = employee.JobTitle,
                    Location = employee.Location,
                    Phone = employee.Phone,
                    Status = (employee.Status)? 0: 1
                };
                _dataService.EmployeeRepository.Add(e);
                _dataService.EmployeeRepository.SaveChanges();
                TempData[TempDataKeys.SUCCESS] = string.Format("Employee '{0}' added successfully.", employee.Name);
                return RedirectToAction("Index");
            }
            else
                ModelState.AddModelError("", "Please check the form for completness.");

            // send back the error
            return View(employee);
        }

        // Edit
        public ActionResult Edit(int Id =0)
        {
            var dbEmp = (from e in _dataService.EmployeeRepository
                         where e.id == Id
                         select e).FirstOrDefault<Employee>();
            if (dbEmp != null)
            {
                EmployeeModel webEmp = new EmployeeModel()
                {
                    Id = dbEmp.id,
                    Name = dbEmp.Name,
                    Email = dbEmp.Email,
                    JobTitle = dbEmp.JobTitle,
                    Location = dbEmp.Location,
                    Phone = dbEmp.Phone,
                    Status = (dbEmp.Status == 0) ? true : false
                };
                return View(webEmp);
            }

            // Go back to the Index and indicate the failure to find the user
            TempData[TempDataKeys.FAILURE] = "Failed to find user.";
            return RedirectToAction("Index");
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeModel employee)
        {
            if (ModelState.IsValid)
            {
                var dbEmp = (from e in _dataService.EmployeeRepository
                             where e.id == employee.Id
                             select e).FirstOrDefault();
                if (dbEmp != null)
                {
                    dbEmp.JobTitle = employee.JobTitle;
                    dbEmp.Location = employee.Location;
                    dbEmp.Email = employee.Email;
                    dbEmp.Phone = employee.Phone;
                    dbEmp.Status = (employee.Status) ? 0 : 1;

                    _dataService.EmployeeRepository.SaveChanges();
                    TempData[TempDataKeys.SUCCESS] = string.Format("Successfully updated '{0}'.", dbEmp.Name);
                    return RedirectToAction("Index");
                }
            }

            // we have errors, so go back to the view
            ModelState.AddModelError("", "Please check for completeness of the form.");
            return View(employee);
        }

        // Delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id= 0)
        {
            // find the user
            Employee dbEmp = (from e in _dataService.EmployeeRepository
                              where e.id == Id
                              select e).FirstOrDefault();
            if (dbEmp != null)
            {
                // begin deletion
                string empName = dbEmp.Name;
                _dataService.EmployeeRepository.Delete(dbEmp);
                _dataService.EmployeeRepository.SaveChanges();
                TempData[TempDataKeys.SUCCESS] = string.Format("Successfully deleted employee: {0}", empName);
                return RedirectToAction("Index");
            }

            // we had a problem
            if (dbEmp == null)
            {
                TempData[TempDataKeys.FAILURE] = "Failed to drop employee.";
                return View("Index");
            }
            else
            {
                EmployeeModel webEmp = new EmployeeModel()
                {
                    Id = dbEmp.id,
                    Name = dbEmp.Name,
                    Location = dbEmp.Location,
                    JobTitle = dbEmp.JobTitle,
                    Email = dbEmp.Email,
                    Status = dbEmp.Status.HasValue ? (dbEmp.Status.Value == 1 ? true: false) : false
                };
                return View("Edit", webEmp);
            }
        }

        // helpers
        public AppUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); }
        }
	}
}