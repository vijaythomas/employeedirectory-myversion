﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HS.ED.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{find}/{page}",
                defaults: new { controller = "Directory", action = "Index", 
                    find = UrlParameter.Optional, page=UrlParameter.Optional}
            );
        }
    }
}
