﻿
use EmployeeDirectory2
go

if not exists( select * from sys.objects where name = 'Employee')
begin
	create table Employee (
		id int identity(1,1) constraint PK_Id Primary Key,
		Name varchar(100) not null,
		JobTitle varchar(100),
		Location varchar(100),
		Email varchar(100),
		Phone varchar(100),
		Status int default 0 )
end
go

if not exists (select * from sys.objects where name = 'Phone')
begin
	create table Phone (
		Id int identity(1,1) constraint PK_Phones_Id Primary Key,
		EmployeeId int constraint FK_Emplopyee_Id references Employee (Id),
		Phone varchar(50) )
end
go

if not exists (select * from sys.objects where name ='User')
begin
	create table [User] (
		Id int identity(1,1) constraint PK_User_id primary key,
		Name varchar(100),
		DisplayName varchar(100),
		Password varchar(100),
		Email varchar(100),
		Status int default 0 )
end
go