﻿use EmployeeDirectory
go

select * from [User]
go

if not exists( select * from [user] where name = 'Admin') begin insert into [User] values ('Admin', 'Administrator', 'password', 'admin@example.com', 0) end
if not exists( select * from [user] where name = 'HRUser') begin insert into [User] values ('HRUser', 'HR User', 'password', 'hrUser@example.com', 0) end
go