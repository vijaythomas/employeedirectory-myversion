﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HS.ED.DataRepository;
using HS.ED.WebUI.Models;
using Ninject;
namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// Validation attribute to ensure that our user name is unique
    /// </summary>
    public class UniqueEmployeeAttribute : ValidationAttribute
    {
        // instances
        IEDDataServices _dataServices;

        // properties
        [Inject]
        public IEDDataServices DataServices { get { return _dataServices; } set { _dataServices = value; } }
        

        // implementation
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // get the object from the context
            EmployeeModel e = (EmployeeModel)validationContext.ObjectInstance;

            // check against the database if this exists
            int counter = (from emps in _dataServices.EmployeeRepository
                             where emps.Name.ToLower() == e.Name.ToLower() 
                                    && emps.Status == 0 
                                    && ((e.Id > 0 && e.Id != emps.id) || (e.Id <= 0))
                             select emps).Count();

            if (counter > 0)
            {
                return (new ValidationResult("The username already exists.", new List<string>(){ validationContext.MemberName}.AsEnumerable<string>()) );
            }

            return ValidationResult.Success;
        }
    }
}