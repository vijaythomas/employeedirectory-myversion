﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// All configuration from the Web.Config
    /// </summary>
    public interface IEDConfiguration
    {
        string GetDBConnectionString();
        int GetMaxItemsPerPage();
        int GetMaxPaginationPages();
    }

    /// <summary>
    /// To channel all configuration entries through the configuration object, thereby making this DI Compatable
    /// </summary>
    public class EDConfiguration : IEDConfiguration
    {
        public string GetDBConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["directoryDB"].ToString();
        }

        public int GetMaxItemsPerPage()
        {
            return int.Parse(ConfigurationManager.AppSettings["MaxItemsPerPage"].ToString());
        }

        public int GetMaxPaginationPages()
        {
            return int.Parse(ConfigurationManager.AppSettings["MaxPaginationPages"].ToString());
        }
    }
}