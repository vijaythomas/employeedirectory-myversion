﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using HS.ED.WebUI.Models;

namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// OWIN DB context is defined here
    /// For now the db connection string is hard coded to IdentityDb
    /// </summary>
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        // static methods for middleware
        static AppIdentityDbContext()
        {
            Database.SetInitializer<AppIdentityDbContext>(new IdentityDbInit());
        }
        public static AppIdentityDbContext Create()
        {
            // call the default constructor
            return new AppIdentityDbContext();
        }

        // constructor
        public AppIdentityDbContext() : base("IdentityDb") { }
    }
    
    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AppIdentityDbContext>
    {
        // seed
        protected override void Seed(AppIdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(AppIdentityDbContext context)
        {
            // all initial configuration goes here
            // first create a usermanager and rolemanager
            AppUserManager userMgr = new AppUserManager(new UserStore<AppUser>(context));
            AppRoleManager roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));

            // add our roles
            List<string> roles = new List<string>(new string[] { "Administrators", "Users" });
            foreach (string role in roles)
            {
                bool roleExists = roleMgr.RoleExists(role);
                if (!roleExists)
                    roleMgr.Create(new AppRole(role));
            }

            // add users
            List<AppUser> users = new List<AppUser>();
            users.Add(new AppUser() { UserName = "Admin", Email = "admin@example.com" });
            users.Add(new AppUser() { UserName = "Staffer", Email = "staffer@example.com" });
            users.Add(new AppUser() { UserName = "pilot", Email = "pilot@example.com" });
            foreach (AppUser user in users)
            {
                AppUser u = userMgr.FindByName(user.UserName);
                if (u == null)
                    userMgr.Create(user, "password");
            }

            // add admin to administrators
            if (!userMgr.IsInRole(users[0].Id, roles[0]))
                userMgr.AddToRole(users[0].Id, roles[0]);

            // add staffer to users
            if (!userMgr.IsInRole(users[1].Id, roles[1]))
                userMgr.AddToRole(users[1].Id, roles[1]);

            // add pilot to users
            if (!userMgr.IsInRole(users[2].Id, roles[1]))
                userMgr.AddToRole(users[2].Id, roles[1]);
        }
    }
}