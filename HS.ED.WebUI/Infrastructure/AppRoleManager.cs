﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using HS.ED.WebUI.Models;

namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// OWIN's Roile Manager with a static method and a constructor
    /// Used in the initialization to create two roles "Administrators" and "Users"
    /// </summary>
    public class AppRoleManager : RoleManager<AppRole>
    {
        // static methods for middle ware
        public static AppRoleManager Create(IdentityFactoryOptions<AppRoleManager> options, IOwinContext context)
        {
            return new AppRoleManager(new RoleStore<AppRole>(context.Get<AppIdentityDbContext>()));
        }

        // constructor
        public AppRoleManager(RoleStore<AppRole> store) : base(store) { }
    }
}