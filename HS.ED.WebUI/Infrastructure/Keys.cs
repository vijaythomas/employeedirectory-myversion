﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// Creating constants - trying to avoid magic strings within the app
    /// </summary>
    public class TempDataKeys
    {
        public const string SUCCESS = "SUCCESSMSG";
        public const string FAILURE = "FAILUREMSG";
        public const string ERRORS = "ERRORMSG";
    }
}