﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using HS.ED.WebUI.Models;

namespace HS.ED.WebUI.Infrastructure
{
    /// <summary>
    /// Implementation of the user manager
    /// Used extensively in the Directory and Account controller
    /// </summary>
    public class AppUserManager : UserManager<AppUser>
    {
        // static constructor and methods for middleware
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            AppIdentityDbContext dbContext = context.Get<AppIdentityDbContext>();
            AppUserManager manager = new AppUserManager(new UserStore<AppUser>(dbContext));

            return manager;
        }

        // constructor
        public AppUserManager(IUserStore<AppUser> store) : base(store) { }

    }
}