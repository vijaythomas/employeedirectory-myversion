﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Mvc;

namespace HS.ED.WebUI.Infrastructure
{
    // purpose is to implement the MVC dependency resolver
    public class NinjectDependencyResolver : IDependencyResolver
    {
        // instances
        private IKernel _kernel;
        
        // constructor
        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }
        
        // fulfilling contact for IDependencyResolver
        public object GetService(Type serviceType)
        {
            return _kernel.Get(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        // setup our bindings here
        private void AddBindings()
        {
            _kernel.Bind<IEDConfiguration>().To<EDConfiguration>();
            _kernel.Bind<HS.ED.DataRepository.IEDDataServices>()
                .To<HS.ED.DataRepository.EDDataServices>()
                .WithConstructorArgument("connectionString", _kernel.Get<IEDConfiguration>().GetDBConnectionString());
        }
    }
}