﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HS.ED.DataRepository.Models;
using HS.ED.WebUI.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace HS.ED.WebUI.Models
{
    /// <summary>
    /// Used within the Directory - to accomodate paging/permissions
    /// </summary>
    public class EmployeeViewModel
    {
        public bool CanEdit { get; set; }
        public bool CanCreate { get; set; }
        public string Find { get; set; }
        public List<Employee> Employees { get; set; }
        public PagingInfo PaginationInfo { get; set; }
    }

    /// <summary>
    /// Used to add/edit an employee to the system
    /// </summary>
    public class EmployeeModel
    {
        public int Id { get; set; }
        [Required]
        [UniqueEmployeeAttribute(ErrorMessage="Employee name is not unique")]
        public string Name { get; set; }
        [Required]
        public string JobTitle { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string Email { get; set; }
        [RegularExpression(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}",ErrorMessage="The Phone number format should be (123)-123-1234")]
        public string Phone { get; set; }
        [Required]
        public bool Status { get; set; }

        public EmployeeModel()
        {
            Status = true;
        }
    }
}