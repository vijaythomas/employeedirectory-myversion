﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HS.ED.WebUI.Models
{
    /// <summary>
    /// The AppUser object - we will use this user object in our app
    /// For now OWIN requires a concrete implemention that can be customized
    /// </summary>
    public class AppUser : IdentityUser
    {
        // we don't need any additional properties for now
    }
}