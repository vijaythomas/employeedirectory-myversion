﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;

namespace HS.ED.WebUI.Models
{
    // OWIN requires a concrete instance of IdentityRole
    public class AppRole : IdentityRole
    {
        // constructors
        public AppRole() : base(){}
        public AppRole(string name) : base(name) { }
    }
}