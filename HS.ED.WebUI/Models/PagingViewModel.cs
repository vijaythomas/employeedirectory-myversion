﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HS.ED.WebUI.Models
{
    /// <summary>
    /// Exclusively used to accomodate paging with the web UI
    /// </summary>
    public class PagingInfo
    {
        public int TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int MaxPaginationPages { get; set; }

        public int TotalPages
        {
            get
            {
                int i = (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
                if (Math.Abs((decimal)TotalItems / ItemsPerPage) > 1)
                    i += (TotalItems % ItemsPerPage) > 0 ? 1 : 0;
                return i;
            }
        }
    }
}