﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HS.ED.WebUI.Models
{
    /// <summary>
    /// The Login View Model - should we add further validations or do client side validations
    /// </summary>
    public class LoginUser
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}