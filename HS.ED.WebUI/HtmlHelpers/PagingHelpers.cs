﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using HS.ED.WebUI.Models;

namespace HS.ED.WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        // generating links for pages - this is the simplest way 
        public static MvcHtmlString PaginationLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pagerUrl)
        {
            StringBuilder result = new StringBuilder();

            // work out the current page for the purpose of pagination
            int currentPage = pagingInfo.CurrentPage <= 1 ? 1 : pagingInfo.CurrentPage -1;

            // calculate the first page number
            int min = (int)Math.Truncate((decimal)currentPage / pagingInfo.MaxPaginationPages);
            min = (min == 0) ? 1 : min;
            if (pagingInfo.CurrentPage > pagingInfo.MaxPaginationPages)
                min = (min * pagingInfo.MaxPaginationPages)+1;                          // should give us 1,11,21,31

            // calculate the max page number
            int max = min + pagingInfo.MaxPaginationPages - 1;
            max = (pagingInfo.TotalPages < max) ? pagingInfo.TotalPages-1 : max;

            // create a list of UL tags
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");

            // generate "prev"
            if (min > pagingInfo.MaxPaginationPages)
                ul.InnerHtml += GeneratePageLink(min - 1, pagerUrl, "&lt;&lt;");

            // generate the pages
            for (int i = min; i <= max; i++)
                ul.InnerHtml += GeneratePageLink(i, pagerUrl);

            // generate "next"
            if (max < pagingInfo.TotalPages-1)
                ul.InnerHtml += GeneratePageLink(max + 1, pagerUrl, "&gt;&gt;");
            
            return MvcHtmlString.Create(ul.ToString());

        }

        // to gereate a string like this <li><a href="/{controller}/{action}/..."></a></li>
        private static string GeneratePageLink(int pageNo, Func<int, string> pageUrl, string displayCharacter = "")
        {
            TagBuilder a = new TagBuilder("a");
            a.MergeAttribute("href", pageUrl(pageNo));
            a.InnerHtml =  (string.IsNullOrEmpty(displayCharacter))? pageNo.ToString() : displayCharacter;

            TagBuilder li = new TagBuilder("li");
            li.InnerHtml = a.ToString();

            return li.ToString();
        }

    }
}