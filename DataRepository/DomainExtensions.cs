﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HS.ED.DataRepository.Models
{
    // extent the Entities class to include a constructor that takes a connection string
    public partial class EmployeeDirectoryEntities
    {
        public EmployeeDirectoryEntities(string connectionString) : base(connectionString) { }
    }
}
