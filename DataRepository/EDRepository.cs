﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HS.ED.DataRepository.Models;

namespace HS.ED.DataRepository
{
    /// <summary>
    /// The Repository interface
    /// </summary>
    /// <typeparam name="TEntity">TEntity of the type HS.ED.DataRepository.Models</typeparam>
    public interface IEDRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Attach(TEntity entity);
        void Delete(TEntity entity);
        void Refresh(TEntity entity);
        IQueryable<TEntity> Select();
        IEnumerable<TEntity> Where(Func<TEntity, bool> predicate);
        int SaveChanges();
    }

    /// <summary>
    /// The concrete EDRepository
    /// </summary>
    /// <typeparam name="TEntity">TEntity of the type HS.ED.DataRepository.Models</typeparam>
    public class EDRepository<TEntity> : IEDRepository<TEntity> where TEntity : class
    {
        //internal properties
        protected EmployeeDirectoryEntities _context;

        //constructor
        public EDRepository(EmployeeDirectoryEntities context)
        {
            _context = context;
        }

        // methods
        public IQueryable<TEntity> Select()
        {
            return _context.Set<TEntity>().AsQueryable();
        }
        public IEnumerable<TEntity> Where(Func<TEntity, bool> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }
        public void Add(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("EDRepository received a null object in add method");
            _context.Set<TEntity>().Add(entity);
        }
        public void Attach(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("EDRepository received a null in the attach method");
            _context.Set<TEntity>().Attach(entity);
        }
        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("EDRepository received a null object in the delete method");
            _context.Set<TEntity>().Remove(entity);
        }
        public void Refresh(TEntity entity)
        {
            if (entity != null)
                _context.Entry<TEntity>(entity).Reload();
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
