﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HS.ED.DataRepository.Models;

namespace HS.ED.DataRepository
{
    public interface IEDDataServices
    {
        IEDRepository<HS.ED.DataRepository.Models.Employee> EmployeeRepository { get; }
        IEDRepository<HS.ED.DataRepository.Models.Phone> PhoneRepository { get; }
        IEDRepository<HS.ED.DataRepository.Models.User> UserRepository { get; }
    }

    public class EDDataServices : IEDDataServices
    {
        //instances
        private EDRepository<Employee> _employeeRepository;
        private EDRepository<User> _userRepository;
        private EDRepository<Phone> _phoneRepository;

        // private
        private string _connectionString;
        private EmployeeDirectoryEntities _context;

        // constructor
        public EDDataServices(string connectionString)
        {
            _connectionString = connectionString;
            _context = new EmployeeDirectoryEntities(_connectionString);
        }

        // properties
        public IEDRepository<Employee> EmployeeRepository
        {
            get
            {
                if (_employeeRepository == null)
                    _employeeRepository = new EDRepository<Employee>(_context);
                return _employeeRepository;
            }
        }
        public IEDRepository<Phone> PhoneRepository
        {
            get
            {
                if (_phoneRepository == null) _phoneRepository = new EDRepository<Phone>(_context);
                return _phoneRepository;
            }
        }
        public IEDRepository<User> UserRepository
        {
            get
            {
                if (_userRepository == null) _userRepository = new EDRepository<User>(_context);
                return _userRepository;
            }
        }
    }
}
