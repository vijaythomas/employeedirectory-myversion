﻿Date: Nov 10, 2015
Author: Vijay Oommen

1. Create the database
	Create a SQL Server database called "EmployeeDirectory2"
2. Create the database schema along with the initial data
	Run the scripts in HS.ED\WebUI\SQL\001.scripts.create.sql in SQL server database
	Run the scripts in HS.ED\WebUI\SQL\002.initialdata.employee.sql in the SQL Server database
	Run the scripts in HS.ED\WebUI\SQL\003.initialdata.user.sql in the SQL server database


3. This project uses Microsoft OWIN (Katana) for authentication and authorization. 
	OWIN uses EF the Code First approach and therefore, when the application is run, the schema for OWIN will be created

4. Change the connection string in the web.config of the HS.ED.WebUI project
	There are two lines:
	<add name="directoryDB" connectionString="metadata=res://*/Models.EmployeeDirectory.csdl|res://*/Models.EmployeeDirectory.ssdl|res://*/Models.EmployeeDirectory.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=(localdb)\Projects;initial catalog=EmployeeDirectory2;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
    <add name="IdentityDb" connectionString="Data Source=(localdb)\Projects;Initial Catalog=EmployeeDirectory2;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;MultipleActiveResultSets=true" providerName="System.Data.SqlClient"/>

	You may need to replace the "Data Source=(localdb)\Projects" to include the server name:
	example:	"Data Source=<SQL-SERVER-NAME>" 

5. Set "HS.ED.WebUI" as the startup project
5. Compile and run the project.

